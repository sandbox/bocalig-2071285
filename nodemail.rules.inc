<?php

/**
 * @file
 * Nodemail Rules Code: actions.
 */

/**
 * Implements hook_rules_aciton_info.
 *
 * Declares any meta-data about actions for Rules.
 */
function nodemail_rules_action_info() {
  $actions = array(
    'nodemail_action_email_whole_node' => array(
      'label' => t('Send an Email containing the whole node'),
      'group' => t('Nodemail'),
      'parameter' => array(
        'to' => array(
          'type' => 'text',
          'label' => t('To'),
          'description' => t('The e-mail address or addresses where the message will be sent to. The formatting of this string must comply with RFC 2822.'),
        ),
        'subject' => array(
          'type' => 'text',
          'label' => t('Subject'),
          'description' => t("The mail's subject."),
          'translatable' => TRUE,
        ),
        'message' => array(
          'type' => 'text',
          'label' => t('Additional Message'),
          'description' => t("An additional mail's message body prepending the node data."),
          'translatable' => TRUE,
        ),
        'node' => array(
          'type' => 'node',
          'label' => t('Node to send.'),
          'description' => t("The node that contains the data to be rendered and sent."),
          'translatable' => TRUE,
        ),
        'from' => array(
          'type' => 'text',
          'label' => t('From'),
          'description' => t("The mail's from address. Leave it empty to use the site-wide configured address."),
          'optional' => TRUE,
        ),
        'language' => array(
          'type' => 'token',
          'label' => t('Language'),
          'description' => t('If specified, the language used for getting the mail message and subject.'),
          'options list' => 'entity_metadata_language_list',
          'optional' => TRUE,
          'default value' => LANGUAGE_NONE,
          'default mode' => 'selector',
        ),
      ),
    ),
  );

  return $actions;
}

/**
 * Function callback to send the email containing the node provided.
 */
function nodemail_action_email_whole_node($to, $subject, $message, $node, $from, $lang) {
  $params['subject'] = $subject;
  $node_view = node_view($node, 'email', $lang);
  $params['body'] = '<p>' . $message . '</p>' . render($node_view);
  $from = $from ? $from : variable_get('site_mail', '');
  $lang = $lang ? $lang : language_default();
  drupal_mail('nodemail', 'node_mail', $to, $lang, $params, $from);
}